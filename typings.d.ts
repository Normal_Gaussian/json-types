declare global {
  type JSONMember = JSONLeaf | JSONNode;
  type JSONLeaf = number | string | boolean | null;
  type JSONNode = JSONArray | JSONObject;
  type JSONArray = Array<JSONMember>;
  type JSONObject = {[key: string]: JSONMember};
  
  type JSONishMember = JSONishLeaf | JSONishNode;
  type JSONishLeaf = undefined | number | string | boolean | null;
  type JSONishNode = JSONishArray | JSONishObject;
  type JSONishArray = Array<JSONishMember>;
  type JSONishObject = {[key: string]: JSONishMember};
}

export var v: void;
