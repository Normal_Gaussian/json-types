# JSONTypes

Typescript definitions for JSON and JSONish (JSON + `undefined`).

## Usage

```ts
import '@normed/json-types'

// Types are now available as globals in this file
//  you do not need to (and cannot) individually import them

const myJSONishObject: JSONishObject = {};

// Allowed
myJSONishObject.x = 3;
myJSONishObject.y = { z: 4 };
myJSONishObject.a = [{b: 2, c: { d: [], e: [], f: 6 }}]
myJSONishObject.x = undefined;
myJSONishObject.y.q = undefined;

// Not allowed
myJSONishObject.d = new Date();

const myJSONObject: JSONObject = {};

// Allowed
myJSONObject.x = 3;
myJSONObject.y = { z: 4 };
myJSONObject.a = [{b: 2, c: { d: [], e: [], f: 6 }}]

// Not allowed
myJSONObject.x = undefined;
myJSONObject.y.q = undefined;
myJSONObject.d = new Date();
```

## The types

Most of the time you want `JSONish` types. The difference is simply that they allow undefined, which exists in JS but not in JSON.

A `<X>Member` is any type - so a `JSONish` member accepts any `JSONish` type and a `JSON` member accepts any `JSON` type.

A `<X>Node` is a type that can have children, so `object` and `Array`. All `JSONNode` types can only have `JSONMember` children.

A `<X>Leaf` is only the types that cannot have children - so the `number`, `string`, `boolean`, and `null` types.

`<X>Array` and `<X>Object` are self-explanatory.

```ts
// JSONish
type JSONishMember = JSONishLeaf | JSONishNode;
type JSONishLeaf = undefined | number | string | boolean | null;
type JSONishNode = JSONishArray | JSONishObject;
type JSONishArray = Array<JSONishMember>;
type JSONishObject = {[key: string]: JSONishMember};

// Plain JSON
type JSONMember = JSONLeaf | JSONNode;
type JSONLeaf = number | string | boolean | null;
type JSONNode = JSONArray | JSONObject;
type JSONArray = Array<JSONMember>;
type JSONObject = {[key: string]: JSONMember};
```
